import { eventBus } from './eventBus'
import axios from 'axios'

const baseUrl = 'http://localhost:8000'

function getAllAppointments () {
  if (eventBus.appointments.length === 0) {
    axios.get(baseUrl + '/appointment/getAll').then(res => {
      eventBus.appointments = res.data
      eventBus.appointmentsId = eventBus.appointments.map(appointment => {
        return appointment.id
      })
    })
  }
}

function getCurrentAppointments () {
  let year = eventBus.daily.currentYear <= 9 ? '0' + eventBus.daily.currentYear : eventBus.daily.currentYear
  let curDate = eventBus.daily.currentDate <= 9 ? '0' + eventBus.daily.currentDate : eventBus.daily.currentDate
  let month = eventBus.daily.currentMonth <= 9 ? '0' + eventBus.daily.currentMonth : eventBus.daily.currentMonth
  let date = year + '-' + month + '-' + curDate
  if (eventBus.appointments.length === 0) {
    axios.get(baseUrl + '/appointment/get/' + date).then(res => {
      eventBus.currentDateAppointments = res.data
    })
  } else if (eventBus.currentDateAppointments.length === 0 || eventBus.currentDateAppointments[0].startDate !== date) {
    eventBus.currentDateAppointments = eventBus.appointments.filter(appointment => {
      return appointment.startDate === date
    })
  }
}

eventBus.$on('changeCurrentDateDaily', ({year, month, date}) => {
  eventBus.daily.currentDate = date
  eventBus.daily.currentMonth = month
  eventBus.daily.currentYear = year
})

eventBus.$on('addAppointment', data => {
  let postData = {
    'title': data.title,
    'all_day': data.allDay ? 1 : 0,
    'startDate': data.starts.date,
    'startTime': data.starts.time,
    'endDate': data.ends.date,
    'endTime': data.ends.time,
    'video_session': data.videoSession ? 1 : 0,
    'appointment_type': data.appointmentType,
    'do_repeat': data.repeat,
    'endRepeat': data.endRepeat,
    'office_location': data.officeLocation,
    'notes': data.notes
  }
  axios.post(baseUrl + '/appointment/post', postData, {
  }).then(res => {
    let date = data.starts.date.split('-')
    let year = parseInt(date[0])
    let month = parseInt(date[1])
    let curDate = parseInt(date[2])
    eventBus.daily.currentDate = curDate
    eventBus.daily.currentMonth = month
    eventBus.daily.currentYear = year
    date = eventBus.daily.currentYear + '-' + eventBus.daily.currentMonth + '-' + eventBus.daily.currentDate
    axios.get(baseUrl + '/appointment/get/' + date).then(res => {
      eventBus.currentDateAppointments = res.data
      for (let i = 0; i < res.data.length; i++) {
        if (eventBus.appointmentsId.indexOf(res.data[i].id) === -1) {
          eventBus.appointments.push(res.data[i])
          eventBus.appointmentsId.push(res.data[i].id)
        }
      }
    })
    eventBus.$emit('reloadPage')
  })
})

eventBus.$on('fetchAllAppointments', () => {
  getAllAppointments()
  getCurrentAppointments()
})

eventBus.$on('fetchCurrentAppointments', () => {
  getCurrentAppointments()
  getAllAppointments()
})

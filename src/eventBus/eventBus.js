import Vue from 'vue'

export const eventBus = new Vue({
  data () {
    return {
      daily: {
        currentDate: new Date().getDate(),
        currentMonth: new Date().getMonth() + 1,
        currentYear: new Date().getFullYear()
      },
      appointments: [],
      currentDateAppointments: [],
      appointmentsId: []
    }
  }
})

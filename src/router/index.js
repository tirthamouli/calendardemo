import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/yearly/Home'
import Daily from '@/components/daily/Home'
import AddAppointment from '@/components/addAppointment/Home'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/daily',
      name: 'Daily',
      component: Daily

    },
    {
      path: '/add-an-appointment',
      name: 'Add',
      component: AddAppointment
    }
  ],
  mode: 'history'
})
